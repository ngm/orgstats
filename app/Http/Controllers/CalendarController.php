<?php

namespace App\Http\Controllers;

use App\OrgParser;

use Illuminate\Http\Request;

class CalendarController extends Controller
{
    public function display()
    {
        $parser = new OrgParser();
        $todos = $parser->getTodos(env('ORG_FILE'));
        $eventsForCalendar = json_encode($todos);

        return view('calendar.display', [
            'events' => $eventsForCalendar
        ]);
    }
}
