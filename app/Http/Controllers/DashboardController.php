<?php

namespace App\Http\Controllers;

use App\OrgStatsCalculator;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $calculator = new OrgStatsCalculator();

        $stats = $calculator->getStats(env('ORG_FILE'));

        return view('dashboard.index', ['stats' => $stats]);
    }
}
