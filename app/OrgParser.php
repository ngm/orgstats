<?php

namespace App;

class Todo
{
    public $title;
    public $start;
}

class OrgParser
{
    public function getTodos($orgFile)
    {
        $orgLines = file($orgFile);

        $todos = [];
        while ($line = current($orgLines)) {
            if (strpos($line, 'TODO') !== false) {
                $isTodo = true;
                $todo = new Todo();
                $todo->title = $line;

                $nextLine = next($orgLines);

                if (strpos($nextLine, 'SCHEDULED') !== false) {
                    preg_match('~<(.*?)>~', $nextLine, $output);
                    $scheduled = $output[1];
                    $datePart = explode(' ', $scheduled);
                    $todo->start = $datePart[0];
                } else {
                    prev($orgLines);
                }

                $todos[] = $todo;
            }

            next($orgLines);
        }

        return $todos;
    }
}
