<?php

namespace App;

class Stats
{
    public $numTodoItems;
}

class OrgStatsCalculator
{
    public function getStats($orgFile)
    {
        $orgLines = file($orgFile);

        $todoItems = preg_grep("/TODO/", $orgLines);
        $doneItems = preg_grep("/DONE/", $orgLines);

        $stats = new Stats();
        $stats->numTodoItems = count($todoItems);
        $stats->numDoneItems = count($doneItems);

        return $stats;
    }
}
