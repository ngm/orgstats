
@extends('layouts.app')

@section('title')
    Calendar
@endsection


@section('content')

<div class="container-fluid">

    <h1>Calendar</h1>

    <div id="calendar">
    </div>

</div>
<!-- /.container-fluid -->

@endsection

@push('scripts')
<script>
$('#calendar').fullCalendar({
    header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,listWeek,basicDay'
    },
    events: {!! $events !!}
});
</script>
@endpush

