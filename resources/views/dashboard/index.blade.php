@extends('layouts.app')

@section('title')
    Dashboard
@endsection


@section('content')

<div class="container-fluid">

    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Overview</li>
    </ol>

    <!-- Icon Cards-->
    <div class="row">
        <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-primary o-hidden h-100">
                <div class="card-body">
                    <div class="card-body-icon">
                        <i class="fas fa-fw fa-list-ul"></i>
                    </div>
                    <div class="display-4 mr-5">{{ $stats->numTodoItems }}</div>
                    <div>TODO items</div>
                </div>
                <!-- <a class="card-footer text-white clearfix small z-1" href="#">
                        <span class="float-left">View Details</span>
                        <span class="float-right">
                        <i class="fas fa-angle-right"></i>
                        </span>
                        </a> -->
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-success o-hidden h-100">
                <div class="card-body">
                    <div class="card-body-icon">
                        <i class="fas fa-fw fa-check-square"></i>
                    </div>
                    <div class="display-4 mr-5">{{ $stats->numDoneItems }}</div>
                    <div>DONE items</div>
                </div>
                <!-- <a class="card-footer text-white clearfix small z-1" href="#">
                        <span class="float-left">View Details</span>
                        <span class="float-right">
                        <i class="fas fa-angle-right"></i>
                        </span>
                        </a> -->
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->

@endsection
