<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{ config('app.name', 'Org Stats') }} - @yield('title')</title>

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>

<body id="page-top">

    @include('layouts.navbar')

    <div id="wrapper">

        <!-- Sidebar -->
        @include('layouts.sidebar')
        <!-- /Sidebar -->

        <div id="content-wrapper">

            @yield('content')

            <!-- Sticky Footer -->
            <footer class="sticky-footer">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>OrgStats - <a href="https://gitlab.com/ngm/orgstats/">gitlab.com/ngm/orgstats</a></span>
                    </div>
                </div>
            </footer>

        </div>
        <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    @include('modals.logout')

    <script src="{{ asset('js/app.js') }}"></script>

    <script src="{{ asset('js/startbootstrap-sb-admin.js') }}"></script>

    @stack('scripts')
</body>

</html>
