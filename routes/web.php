<?php

Route::get('/', function() {
    return redirect('/dashboard');
});

Route::get('/dashboard', 'DashboardController@index')->middleware('auth');

Route::get('/calendar', 'CalendarController@display')->middleware('auth');

Auth::routes();
