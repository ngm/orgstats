<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CalendarTest extends TestCase
{
    /** @test */
    public function no_scheduled_todos()
    {
        // Given there are no scheduled todos

        // When I visit the todos calendar page
        $response = $this->get('/calendar');

        // Then I should see an empty calendar
        $response->assertStatus(200);
        $response->assertSeeText('Calendar');

        $response->assertSee('<div id="calendar">');
        // Can't test much on the calendar here because it's
        // initialised with JS.  Will need to use Dusk to test.
    }
}
