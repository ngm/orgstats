<?php

namespace Tests\Unit;

use App\OrgStatsCalculator;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CalculateStatsTest extends TestCase
{
    /** @test */
    public function one_todo_item()
    {
        // given we have an org file with one TODO
        $orgString = "* TODO World peace";
        $orgFile = "test.org";
        file_put_contents($orgFile, $orgString);

        // when we parse it for stats
        $calculator = new OrgStatsCalculator();
        $stats = $calculator->getStats($orgFile);

        $numTodoItems = $stats->numTodoItems;

        // we should be told we have 1 TODO item
        $this->assertEquals(1, $numTodoItems);
    }

    /** @test */
    public function two_todo_items()
    {
        // given we have an org file with one TODO
        $orgString = "* TODO World peace\n";
        $orgString .= "* TODO Buy milk";
        $orgFile = "test.org";
        file_put_contents($orgFile, $orgString);

        // when we parse it for stats
        $calculator = new OrgStatsCalculator();
        $stats = $calculator->getStats($orgFile);
        $numTodoItems = $stats->numTodoItems;

        // we should be told we have 1 TODO item
        $this->assertEquals(2, $numTodoItems);
    }

    /** @test */
    public function one_item_with_some_subtext()
    {
        // given we have an org file with one TODO
        $orgString  = "* TODO World peace\n";
        $orgString .= "  DEADLINE: <2100-12-31>";
        $orgFile = "test.org";
        file_put_contents($orgFile, $orgString);

        // when we parse it for stats
        $calculator = new OrgStatsCalculator();
        $stats = $calculator->getStats($orgFile);
        $numTodoItems = $stats->numTodoItems;

        // we should be told we have 1 TODO item
        $this->assertEquals(1, $numTodoItems);
    }

    /** @test */
    public function one_item_with_some_subtext_one_done_item()
    {
        // given we have an org file with one TODO
        $orgString  = "* DONE World peace\n";
        $orgString .= "  DEADLINE: <2100-12-31>\n";
        $orgString .= "* TODO Buy milk";
        $orgFile = "test.org";
        file_put_contents($orgFile, $orgString);

        // when we parse it for stats
        $calculator = new OrgStatsCalculator();
        $stats = $calculator->getStats($orgFile);
        $numTodoItems = $stats->numTodoItems;
        $numDoneItems = $stats->numDoneItems;

        // we should be told we have 1 TODO item
        $this->assertEquals(1, $numTodoItems);
        $this->assertEquals(1, $numDoneItems);
    }
}
