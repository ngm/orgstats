<?php

namespace Tests\Unit;

use App\OrgParser;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ParseScheduledDatesTest extends TestCase
{
    /** @test */
    public function todo_item_with_scheduled_date()
    {
        // given we have an org file with one TODO
        $orgString = "* TODO World peace";
        $orgString .= "\n";
        $orgString .= "SCHEDULED <2018-08-11 Sat>";
        $orgFile = "test.org";
        file_put_contents($orgFile, $orgString);

        // when we parse it out of the file
        $parser = new OrgParser();

        $todos = $parser->getTodos($orgFile);

        $scheduledDate = $todos[0]->scheduled;

        // we should be told we have 1 TODO item
        $this->assertEquals('2018-08-11', $scheduledDate);
    }

    /** @test */
    public function one_todo_item_with_no_date_one_with_scheduled_date()
    {
        // given we have an org file with one TODO
        $orgString = "* TODO World peace";
        $orgString .= "\n";
        $orgString = "* TODO Buy milk";
        $orgString .= "\n";
        $orgString .= "SCHEDULED <2018-08-11 Sat>";
        $orgFile = "test.org";
        file_put_contents($orgFile, $orgString);

        // when we parse it out of the file
        $parser = new OrgParser();

        $todos = $parser->getTodos($orgFile);

        $scheduledDate = $todos[0]->scheduled;

        // we should be told we have 1 TODO item
        $this->assertEquals('2018-08-11', $scheduledDate);
    }
}
