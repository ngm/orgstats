Feature: Calendar of todos

In order to organise work on upcoming todos
As an org user
I would like to see a calendar view of scheduled todos and those with deadlines

Scenario: No scheduled todos
  Given there are no scheduled todos
  When I visit the todos calendar page
  Then I should see an empty calendar
